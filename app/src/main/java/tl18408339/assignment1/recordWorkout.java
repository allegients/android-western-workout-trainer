/*
Timon Leung (18408339)
Java class of the record workout screen
Version 1.04
 */

package tl18408339.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class recordWorkout extends AppCompatActivity {
    ScrollView scrollView;
    DatabaseManager mydManager;
    ListView exerciseList;
    String weekDay;
    Spinner category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_workout);

        mydManager = new DatabaseManager(recordWorkout.this);
        exerciseList = (ListView) findViewById(R.id.exerciseList);
        category = (Spinner) findViewById(R.id.todayCategory);
        TextView day = (TextView) findViewById(R.id.todayDay);

        /*
        The following code was inspired from https://stackoverflow.com/questions/18256521/android-calendar-get-current-day-of-week-as-string
        Grabbing today's day
         */
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
        Calendar calendar = Calendar.getInstance();
        weekDay = dayFormat.format(calendar.getTime());     // End of code
        day.setText(weekDay);
        String getDay = "day='" + weekDay + "'";    // whereCLAUSE

        /*
        The following code was modified from:
        https://inducesmile.com/android/populating-android-spinner-from-sqlite-database/
        Placing data into spinners to show today's categories
        */
        String[] spinnerLists = mydManager.retrieveCategories(getDay);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(recordWorkout.this, android.R.layout.simple_spinner_item, spinnerLists);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(spinnerAdapter);
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showRec();
                return;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }); // End of code

        exerciseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View
                    v, int position, long id) {
                Intent intent = new Intent(recordWorkout.this, editWorkout.class);
                String item = (String) exerciseList.getAdapter().getItem(position);
                if (item.contains(" ")) {       // Obtaining exerciseName
                    item = item.substring(0, item.indexOf(" ,    "));   // When to cut off the string
                    String exerciseID = item;
                    intent.putExtra("exerciseName", exerciseID);

                }
                startActivity(intent);
            }
        });
    }

    /* The following code was modified from Lecture 6 on vUWS
    Saving the position of scrollview when the device is rotated
    */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position =
                savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        if (position != null) scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(position[0], position[1]);
            }
        });
    }       // End of code

    /*
    The following code was modified from Lecture 5 materias on vUWS
    Querying SQL and placing results into listview
     */
    public boolean showRec() {
        mydManager.openReadable();
        String chosenCategory = category.getSelectedItem().toString();
        String whereCLAUSE = "day='" + weekDay + "' AND category='" + chosenCategory + "'";
        ArrayList<String> tableContent = mydManager.retrieveRows(whereCLAUSE);
        ArrayAdapter<String> arrayAdpt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tableContent);
        exerciseList.setAdapter(arrayAdpt);
        return true;
    }       // End of code
}       // End of recordWorkout.java

