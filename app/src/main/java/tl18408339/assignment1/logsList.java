/*
Timon Leung (18408339)
Java class of list view for logs of chosen date
Version 1.0
 */

package tl18408339.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class logsList extends AppCompatActivity {
    ScrollView scrollView;
    DatabaseManager mydManager;
    String chosenDate;
    ListView logsList;
    SendSMS mSender = new SendSMS();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs_list);
        mydManager = new DatabaseManager(logsList.this);
        logsList = (ListView) findViewById(R.id.workLogsList);

        Intent intent = getIntent();
        chosenDate = intent.getStringExtra("date");
        showLogs();
    }

    // Query database
    public boolean showLogs() {
        mydManager.openReadable();
        String whereCLAUSE = "date='" + chosenDate + "'";
        ArrayList<String> tableContent = mydManager.retrieveLogs(whereCLAUSE);
        ArrayAdapter<String> arrayAdpt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tableContent);
        logsList.setAdapter(arrayAdpt);
        return true;
    }

    /* The following code was inspired from Lecture 5 materias on vUWS
     Send friend a SMS
     */
    public void textFriend(View v) {
        boolean success = mSender.sendSMSMessage("647-000-0000",
                // This is standard lorem-ipsum text, do not bother
                // trying to wrap it, there's about 500 characters...
                "I am breaking records with my gym worksouts!"
        );
        Toast.makeText(this, "Message sent " + (
                        success ? "successfully" : "unsuccessfully"),
                Toast.LENGTH_SHORT).show();
    }   // End of code

    /* The following code was modified from Lecture 6 on vUWS
Saving the position of scrollview when the device is rotated
*/
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position =
                savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        if (position != null) scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(position[0], position[1]);
            }
        });
    }       // End of code
}       // End of logsList.java
