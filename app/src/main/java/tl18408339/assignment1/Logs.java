/*
Timon Leung (18408339)
Java class of when user chooses date to show logs
Version 1.0
 */

package tl18408339.assignment1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.view.View.OnClickListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class Logs extends Fragment {


    public Logs() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logs, container, false);

        Button button = (Button) view.findViewById(R.id.logButton);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Grabbing chosen date and putting it into a string to query database
                DatePicker datePicker = (DatePicker) getView().findViewById(R.id.datePicker);
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();
                String date = Integer.toString(day) + "-" + Integer.toString(month) + "-" + Integer.toString(year);
                Intent intent = new Intent(getActivity(), logsList.class);
                intent.putExtra("date", date);
                startActivity(intent);
            }
        });
        return view;
    }

    public static Logs newInstance() {
        Logs fragment = new
                Logs();
        return fragment;
    }
}       // End of Logs.java
