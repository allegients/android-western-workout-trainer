/*
Timon Leung (18408339)
Java class of the set up exercise screen
Version 1.03
 */

package tl18408339.assignment1;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.Spinner;

public class setUpExercises extends AppCompatActivity {

    NumberPicker repPicker, setPicker, weightPicker;
    ScrollView scrollView;
    EditText exerciseTxt;
    Spinner exerciseSpinner;
    Button insertExercise;
    Boolean recInserted;
    DatabaseManager mydManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_exercises);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Stop keyboard from auto popping up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Setting min and max values of all the number pickers
        repPicker = (NumberPicker) findViewById(R.id.noReptitions);
        repPicker.setMaxValue(1000);
        repPicker.setMinValue(0);
        repPicker.setWrapSelectorWheel(false);

        setPicker = (NumberPicker) findViewById(R.id.noSets);
        setPicker.setMaxValue(1000);
        setPicker.setMinValue(0);
        setPicker.setWrapSelectorWheel(false);

        weightPicker = (NumberPicker) findViewById(R.id.initialWeight);
        weightPicker.setMaxValue(1000);
        weightPicker.setMinValue(0);
        weightPicker.setWrapSelectorWheel(false);

        // Validation on whether all inputs were entered
        insertExercise = (Button) findViewById(R.id.addButton);
        insertExercise.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(setUpExercises.this);
                boolean validation = true;
                exerciseTxt = (EditText) findViewById(R.id.exerciseName);
                repPicker = (NumberPicker) findViewById(R.id.noReptitions);
                setPicker = (NumberPicker) findViewById(R.id.noSets);
                weightPicker = (NumberPicker) findViewById(R.id.initialWeight);
                exerciseSpinner = (Spinner) findViewById(R.id.exerciseCategoryChooser);
                String exerciseName = exerciseTxt.getText().toString();
                String exerciseCategory = exerciseSpinner.getSelectedItem().toString();
                int rep = repPicker.getValue();
                int set = setPicker.getValue();
                int weight = weightPicker.getValue();

                if (TextUtils.isEmpty(exerciseName)) {
                    validation = false;
                }

                if (exerciseCategory.equals("Select Category")) {
                    validation = false;
                }

                if ((rep == 0) || (set == 0) || (weight == 0)) {
                    validation = false;
                }

                if (!validation) {
                    alertDialog.setTitle("ERROR!");
                    alertDialog.setMessage("Please enter data for all inputs!");
                    alertDialog.setPositiveButton("OK", null);
                    alertDialog.show();
                } else {

                    /* The following code was modified from Lecture 5 on vUWS
                    Inserting data into database
                    */
                    mydManager = new DatabaseManager(setUpExercises.this);
                    recInserted = mydManager.addExercise(exerciseName, exerciseCategory, set, rep, weight, null);
                    if (recInserted) {
                        alertDialog.setMessage("Successfully added exercise!");
                        alertDialog.setPositiveButton("OK", null);
                        alertDialog.show();
                    } else {
                        alertDialog.setTitle("ERROR!");
                        alertDialog.setMessage("Inserting data failed");
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(exerciseTxt.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    mydManager.close();
                    resetInputs();
                }       // End of code
            }
        });

    }


    /* The following code was modified from Lecture 6 on vUWS
       Saving the position of scrollview when the device is rotated
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position =
                savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        if (position != null) scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(position[0], position[1]);
            }
        });
    }       // End of code

    public void resetButton(View v) {
        resetInputs();
    }

    // Resets all the user inputs
    public void resetInputs() {
        exerciseTxt = (EditText) findViewById(R.id.exerciseName);
        exerciseSpinner = (Spinner) findViewById(R.id.exerciseCategoryChooser);
        exerciseTxt.setText("");
        repPicker.setValue(0);
        setPicker.setValue(0);
        weightPicker.setValue(0);
        exerciseSpinner.setSelection(0);
    }
}

// End of setUpExercises.java