/*
Timon Leung (18408339)
Java class of the editWorkout screen
Version 1.03
 */

package tl18408339.assignment1;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class editWorkout extends AppCompatActivity {
    DatabaseManager mydManager;
    NumberPicker repPicker, weightPicker;
    int sets, reps, weight, setNumber = 1;
    TextView setCount;
    ScrollView scrollView;
    String getTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workout);

        // Stop keyboard from auto popping up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mydManager = new DatabaseManager(editWorkout.this);
        TextView title = (TextView) findViewById(R.id.editExerciseID);
        setCount = (TextView) findViewById(R.id.setNumber);

        repPicker = (NumberPicker) findViewById(R.id.repNumber);
        repPicker.setMaxValue(1000);
        repPicker.setMinValue(1);
        weightPicker = (NumberPicker) findViewById(R.id.weightNumber);
        weightPicker.setMaxValue(1000);
        weightPicker.setMinValue(1);

        Intent intent = getIntent();
        getTitle = intent.getStringExtra("exerciseName");
        String WHEREclause = "exerciseName='" + getTitle + "'";

        // Getting value of set, rep and weight of exercise
        sets = mydManager.retrieveSets(WHEREclause);
        reps = mydManager.retrieveReps(WHEREclause);
        weight = mydManager.retrieveWeight(WHEREclause);

        title.setText(getTitle);
        setCount.setText("You are currently on set " + setNumber + "/" + sets);
        repPicker.setValue(reps);
        weightPicker.setValue(weight);
    }

    // Method to save workout
    public void save(View v) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(editWorkout.this);
        if (setNumber == sets) {
            alertDialog.setMessage("You have successfully completed all sets for this exercise!");
            alertDialog.setPositiveButton("OK", null);
            alertDialog.show();
        } else {
            String date = new SimpleDateFormat("d-M-yyyy").format(new Date());
            int updatedRep = repPicker.getValue();  // Grabbing values in case user changed
            int updatedWeight = weightPicker.getValue();
            boolean logsInserted = mydManager.addLog(date, getTitle, setNumber, updatedRep, updatedWeight);
            if (logsInserted) {
                /*
                The following code was modified from https://stackoverflow.com/questions/42128173/countdown-in-alertdialog-android-studio
                Count down timer
                 */
                final AlertDialog.Builder builder = new AlertDialog.Builder(editWorkout.this);
                builder.setTitle("Rest time");
                builder.setMessage("start time");
                builder.setPositiveButton("Skip rest", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                final AlertDialog alert = builder.create();
                alert.show();
                new CountDownTimer(60000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        alert.setMessage("00:" + (millisUntilFinished / 1000));
                    }

                    @Override
                    public void onFinish() {
                        alert.setMessage("Rest time over! Next set");
                    }
                }.start();      // End of code

                setNumber++;
                repPicker.setValue(reps);
                weightPicker.setValue(weight);
                setCount.setText("You are currently on set " + setNumber + "/" + sets);
            } else {
                alertDialog.setTitle("ERROR!");
                alertDialog.setMessage("Something went wrong!");
            }
        }
    }

    // If user decide to increase number of sets
    public void setIncrease(View v) {
        sets++;
        setCount.setText("You are currently on set " + setNumber + "/" + sets);
    }


    /* The following code was modified from Lecture 6 on vUWS
    Saving the position of scrollview when the device is rotated
    */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position =
                savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        if (position != null) scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(position[0], position[1]);
            }
        });
    }       // End of code
}       // End of editWorkout.java
