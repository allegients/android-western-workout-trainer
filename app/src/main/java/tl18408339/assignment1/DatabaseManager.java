/*
Timon Leung (18408339)
    The following code was modified from Lecture 5 Materials on vUWS 'DatabaseManager.java'
Database manager for the app
Version 1.06
*/

package tl18408339.assignment1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseManager {

    // Database name
    public static final String DB_NAME = "westernTrainer";

    // Table names
    public static final String TABLE_EXERCISE = "exercise";
    public static final String TABLE_LOG = "log";
    public static final int DB_VERSION = 1;

    // Exercise table creation
    private static final String CREATE_TABLE_EXERCISE = "CREATE TABLE " + TABLE_EXERCISE + " (exerciseName TEXT, category TEXT, sets INTEGER, reps INTEGER, weight Integer, day TEXT);";

    // Log table creation
    private static final String CREATE_TABLE_LOG = "CREATE TABLE " + TABLE_LOG + " (date TEXT, exercise TEXT, setNumber INTEGER, repsLogs INTEGER, weightLogs Integer);";

    private SQLHelper helper;
    private SQLiteDatabase db;
    private Context context;

    // Opening database to write
    public DatabaseManager(Context c) {
        this.context = c;
        helper = new SQLHelper(c);
        this.db = helper.getWritableDatabase();
    }

    // Opening database to read
    public DatabaseManager openReadable() throws android.database.SQLException {
        helper = new SQLHelper(context);
        db = helper.getReadableDatabase();
        return this;
    }

    // Closing database
    public void close() {
        helper.close();
    }

    // Method to add values to exercise table
    public boolean addExercise(String n, String c, Integer s, Integer r, Integer w, String d) {
        synchronized (this.db) {

            ContentValues newExercise = new ContentValues();
            newExercise.put("exerciseName", n);
            newExercise.put("category", c);
            newExercise.put("sets", s);
            newExercise.put("reps", r);
            newExercise.put("weight", w);
            newExercise.put("day", d);
            try {
                db.insertOrThrow(TABLE_EXERCISE, null, newExercise);
            } catch (Exception e) {
                Log.e("Error in inserting rows", e.toString());
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

    // Update table to add days for workout program
    public boolean addDays(String day, String exercise) {
        synchronized (this.db) {
            ContentValues newWorkout = new ContentValues();
            newWorkout.put("day", day);
            try {
                int rowsUpdated = db.update(TABLE_EXERCISE, newWorkout, exercise, null);
            } catch (Exception e) {
                Log.e("Error in updating rows", e.toString());
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

    // Getting number of sets for an exercise
    public int retrieveSets(String exercise) {
        int setsRows = 0;
        String[] columns = new String[]{"sets"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, exercise, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            setsRows = (cursor.getInt(0));
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return setsRows;
    }

    // Getting initial weight for an exercise
    public int retrieveWeight(String exercise) {
        int weightRows = 0;
        String[] columns = new String[]{"weight"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, exercise, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            weightRows = (cursor.getInt(0));
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return weightRows;
    }

    // Getting number of reps for an exercise
    public int retrieveReps(String exercise) {
        int repsRows = 0;
        String[] columns = new String[]{"reps"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, exercise, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            repsRows = (cursor.getInt(0));
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return repsRows;
    }

    /*
    The following code was modified from:
    https://inducesmile.com/android/populating-android-spinner-from-sqlite-database/
    Method to retrieve exercises from a certain category
     */
    public String[] retrieveExercises(String category) {
        ArrayList<String> exerciseRows = new ArrayList<String>();
        String[] columns = new String[]{"exerciseName"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, category, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            exerciseRows.add(cursor.getString(0));
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        String[] spinnerCategory = new String[exerciseRows.size()];
        spinnerCategory = exerciseRows.toArray(spinnerCategory);

        return spinnerCategory;
    }   // End of code

    // Finding all categories which have a daily program on the day
    public String[] retrieveCategories(String day) {
        ArrayList<String> categoryRows = new ArrayList<String>();
        String[] columns = new String[]{"DISTINCT(category)"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, day, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            categoryRows.add(cursor.getString(0));
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        String[] spinnerCategory = new String[categoryRows.size()];
        spinnerCategory = categoryRows.toArray(spinnerCategory);

        return spinnerCategory;
    }

    // Getting all exercises and their data from a daily program
    public ArrayList<String> retrieveRows(String whereClause) {
        ArrayList<String> exerciseRows = new ArrayList<String>();
        String[] columns = new String[]{"exerciseName", "sets", "reps", "weight"};
        Cursor cursor = db.query(TABLE_EXERCISE, columns, whereClause, null, null, null, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            exerciseRows.add(cursor.getString(0) + " ,    \nSETS:  " + Integer.toString(cursor.getInt(1)) + " \nREPS: " + Integer.toString(cursor.getInt(2)) + " \nWEIGHT: " + Integer.toString(cursor.getInt(3)) + "KG");
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return exerciseRows;
    }

    // Method to add values to logs table
    public boolean addLog(String d, String n, Integer s, Integer r, Integer w) {
        synchronized (this.db) {

            ContentValues newLog = new ContentValues();
            newLog.put("date", d);
            newLog.put("exercise", n);
            newLog.put("setNumber", s);
            newLog.put("repsLogs", r);
            newLog.put("weightLogs", w);

            try {
                db.insertOrThrow(TABLE_LOG, null, newLog);
            } catch (Exception e) {
                Log.e("Error in inserting rows", e.toString());
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

    // Getting all workouts done on a date
    public ArrayList<String> retrieveLogs(String date) {
        ArrayList<String> logsRows = new ArrayList<String>();
        String[] columns = new String[]{"date", "exercise", "setNumber", "repsLogs", "weightLogs"};
        Cursor cursor = db.query(TABLE_LOG, columns, date, null, null, null, "setNumber");
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            logsRows.add(cursor.getString(0) + " \nExercise:  " + cursor.getString(1) + "\nSet Number: " + Integer.toString(cursor.getInt(2)) +
                    " \nReps: " + Integer.toString(cursor.getInt(3)) + " \nWeight: " + Integer.toString(cursor.getInt(4)) + " KG");
            cursor.moveToNext();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return logsRows;
    }

    public void clearRecords() {
        db = helper.getWritableDatabase();
        db.delete(TABLE_EXERCISE, null, null);
        db.delete(TABLE_LOG, null, null);

    }

    public class SQLHelper extends SQLiteOpenHelper {
        public SQLHelper(Context c) {
            super(c, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Creating tables
            db.execSQL(CREATE_TABLE_EXERCISE);
            db.execSQL(CREATE_TABLE_LOG);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("Products table", "Upgrading database i.e. dropping table and re-creating it");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
            onCreate(db);
        }
    }
}   // End of DatabaseManager.java