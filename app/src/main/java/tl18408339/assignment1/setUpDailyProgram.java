/*
Timon Leung (18408339)
Java class of the setting up daily program screen
Version 1.05
 */

package tl18408339.assignment1;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import org.w3c.dom.Text;


public class setUpDailyProgram extends AppCompatActivity {
    ScrollView scrollView;
    DatabaseManager mydManager;
    Spinner exerciseCategory, day, exercise1, exercise2, exercise3, exercise4, exercise5, exercise6;
    String getCategory, getDay;
    NumberPicker noExercises;
    Boolean dayUpdated;
    TextView exercise2Text, exercise3Text, exercise4Text, exercise5Text, exercise6Text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_daily_program);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        exerciseCategory = (Spinner) findViewById(R.id.categoryChooser);
        exercise1 = (Spinner) findViewById(R.id.exercise1);
        exercise2 = (Spinner) findViewById(R.id.exercise2);
        exercise3 = (Spinner) findViewById(R.id.exercise3);
        exercise4 = (Spinner) findViewById(R.id.exercise4);
        exercise5 = (Spinner) findViewById(R.id.exercise5);
        exercise6 = (Spinner) findViewById(R.id.exercise6);
        day = (Spinner) findViewById(R.id.dayChooser);
        noExercises = (NumberPicker) findViewById(R.id.noExercises);
        mydManager = new DatabaseManager(setUpDailyProgram.this);
        noExercises.setMaxValue(6);
        noExercises.setMinValue(1);
        final Button save = (Button) findViewById(R.id.saveProgram);

        // Listener for when the category spinner is changed
        exerciseCategory.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // Making sure a category is chosen before showing button to save
                String categoryString = exerciseCategory.getSelectedItem().toString();
                if (categoryString.equals("Select Category")) {
                    save.setVisibility(View.INVISIBLE);
                } else {
                    save.setVisibility(View.VISIBLE);
                }
                getCategory = "category='" + categoryString + "'";

                /*
                The following code was modified from:
                https://inducesmile.com/android/populating-android-spinner-from-sqlite-database/
                Placing data into spinners
                 */
                String[] spinnerLists = mydManager.retrieveExercises(getCategory);
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(setUpDailyProgram.this, android.R.layout.simple_spinner_item, spinnerLists);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                exercise1.setAdapter(spinnerAdapter);
                exercise2.setAdapter(spinnerAdapter);
                exercise3.setAdapter(spinnerAdapter);
                exercise4.setAdapter(spinnerAdapter);
                exercise5.setAdapter(spinnerAdapter);
                exercise6.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    /* The following code was modified from Lecture 6 on vUWS
       Saving the position of scrollview when the device is rotated
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position =
                savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        scrollView = (ScrollView) findViewById(R.id.scrollwid);
        if (position != null) scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(position[0], position[1]);
            }
        });
    }   // End of code

    // Changing whether the spinners are visible depending on number of exercises
    public void changeExerciseNo(View v) {
        int amount = noExercises.getValue();
        exercise2Text = (TextView) findViewById(R.id.textView8);
        exercise3Text = (TextView) findViewById(R.id.textView9);
        exercise4Text = (TextView) findViewById(R.id.textView10);
        exercise5Text = (TextView) findViewById(R.id.textView11);
        exercise6Text = (TextView) findViewById(R.id.textView12);

        if (amount >= 2) {
            exercise2.setVisibility(View.VISIBLE);
            exercise2Text.setVisibility(View.VISIBLE);
        } else {
            exercise2.setVisibility(View.INVISIBLE);
            exercise2Text.setVisibility(View.INVISIBLE);
        }

        if (amount >= 3) {
            exercise3.setVisibility(View.VISIBLE);
            exercise3Text.setVisibility(View.VISIBLE);
        } else {
            exercise3.setVisibility(View.INVISIBLE);
            exercise3Text.setVisibility(View.INVISIBLE);
        }

        if (amount >= 4) {
            exercise4.setVisibility(View.VISIBLE);
            exercise4Text.setVisibility(View.VISIBLE);
        } else {
            exercise4.setVisibility(View.INVISIBLE);
            exercise4Text.setVisibility(View.INVISIBLE);
        }

        if (amount >= 5) {
            exercise5.setVisibility(View.VISIBLE);
            exercise5Text.setVisibility(View.VISIBLE);
        } else {
            exercise5.setVisibility(View.INVISIBLE);
            exercise5Text.setVisibility(View.INVISIBLE);
        }

        if (amount >= 6) {
            exercise6.setVisibility(View.VISIBLE);
            exercise6Text.setVisibility(View.VISIBLE);
        } else {
            exercise6.setVisibility(View.INVISIBLE);
            exercise6Text.setVisibility(View.INVISIBLE);
        }
    }

    // Updating days of exercises chosen
    public void submitWorkout(View v) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(setUpDailyProgram.this);
        alertDialog.setPositiveButton("OK", null);
        int amount = noExercises.getValue();
        getDay = day.getSelectedItem().toString();

        if (amount >= 1) {
            String getExercise = "exerciseName='" + exercise1.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (amount >= 2) {
            String getExercise = "exerciseName='" + exercise2.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (amount >= 3) {
            String getExercise = "exerciseName='" + exercise3.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (amount >= 4) {
            String getExercise = "exerciseName='" + exercise4.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (amount >= 5) {
            String getExercise = "exerciseName='" + exercise5.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (amount >= 6) {
            String getExercise = "exerciseName='" + exercise6.getSelectedItem().toString() + "'";
            dayUpdated = mydManager.addDays(getDay, getExercise);
        }

        if (dayUpdated) {
            alertDialog.setMessage("You have successfully added the workout program!");
            alertDialog.show();
        }
    }
}

// End of setUpDailyProgram.java
